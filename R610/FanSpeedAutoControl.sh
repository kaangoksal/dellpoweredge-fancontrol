#!/bin/bash
# this version is for PowerEdge R610, we have only one ambient temperature sensor in R610


# executes this script every minute
# * * * * * /bin/bash /root/fanControl.sh >> /tmp/temp_cron.log

# place this with crontab -e

#enable manual fan control
#ipmitool raw 0x30 0x30 0x01 0x00 
# decrease the fan speeds to %15. the last word is hex number
#ipmitool raw 0x30 0x30 0x02 0xff 0x0f

# turn on auto fan speeds
#ipmitool raw 0x30 0x30 0x01 0x01


TEMPTHRESHOLD=27

T=$(ipmitool sdr type temperature | grep Ambient | cut -d"|" -f5 | cut -d" " -f2)

AMBIENTTEMP=$(echo $T | cut -d" " -f3)

DATE=$(date +%Y-%m-%d-%H:%M:%S)

echo "[$DATE] Ambient: $AMBIENTTEMP"

if [[ $AMBIENTTEMP -gt TEMPTHRESHOLD ]];
then
    echo "Temperature threshold reached! Activating fans"
    ipmitool raw 0x30 0x30 0x01 0x01 #auto fan speed
else
    echo "CPU Tempreatures are below threshold, silencing fans"
    ipmitool raw 0x30 0x30 0x01 0x00 #manual fan speed active
    ipmitool raw 0x30 0x30 0x02 0xff 0x14 #set fan speed to 10%
fi

