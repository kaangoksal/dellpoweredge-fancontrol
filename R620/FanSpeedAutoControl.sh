#!/bin/bash
# this version is for PowerEdge R620


# executes this script every minute
# * * * * * /bin/bash /root/test_script.sh >> /tmp/temp_cron.log

# place this with crontab -e

#enable manual fan control
#ipmitool raw 0x30 0x30 0x01 0x00 
# decrease the fan speeds to %15. the last word is hex number
#ipmitool raw 0x30 0x30 0x02 0xff 0x0f

# turn on auto fan speeds
#ipmitool raw 0x30 0x30 0x01 0x01


TEMPTHRESHOLD=55

T=$(ipmitool sdr type temperature | cut -d"|" -f5 | cut -d" " -f2)

INLETTEMP=$(echo $T | cut -d" " -f1)
EXHAUSTTEMP=$(echo $T | cut -d" " -f2)
CPUTEMP1=$(echo $T | cut -d" " -f3)
CPUTEMP2=$(echo $T | cut -d" " -f4)
DATE=$(date +%Y-%m-%d-%H:%M:%S)

echo "[$DATE] Inlet: $INLETTEMP Exhaust: $EXHAUSTTEMP CPU1: $CPUTEMP1 CPU2: $CPUTEMP2"

if [[ $CPUTEMP1 -gt TEMPTHRESHOLD  || $CPUTEMP2 -gt TEMPTHRESHOLD ]];
then
    echo "Temperature threshold reached! Activating fans"
    ipmitool raw 0x30 0x30 0x01 0x01 #auto fan speed
else
    echo "CPU Tempreatures are below threshold, silencing fans"
    ipmitool raw 0x30 0x30 0x01 0x00 #manual fan speed active
    ipmitool raw 0x30 0x30 0x02 0xff 0x0A #set fan speed to 10%
fi


